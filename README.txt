

REQUIREMENTS
------------

Product case tracker module is dependent on ubercart module's
uc_product - http://drupal.org/project/ubercart



INSTALLATION
------------

  - Download and extract the module files.
  - Enable the module from /admin/modules. Module will only
    get enabled if uc_product is enabled.


GUIDING STEPS
-------------

  > Are there any ubercart product classes in
    admin/store/products/classes ?

  > If no, then let's create a ubercart class type called new_test.

  > After creating new_test, go to 
    admin/structure/types/manage/new_test/fields to set a status field on which
    the product case tracker will work. This status field should strictly be a
    list(text) field type with widget named select list only.

  > Now go to the configuration page at admin/store/settings/case_tracker to
    link this newly created select field.

  > After enabling new_text checkbox, you will get a dropdown of all the select
    type fields in new_test. Select one that you think will do.

  -----------------------------------------------------------------------------
  |  NOTE : Only one field can be linked with new_test. If you want to link   |
  |  another field, you need to create a new ubercart class type.             |
  -----------------------------------------------------------------------------

  > Now select the roles to be assigned to product designer and customer. These
    roles are necessary for selecting names of product designers, customers, or
    collaborators from auto_complete fields. Save this configuration settings.

  > Add a new content at node/add/new-test. Click Save or Save and continue.

  > There will now be a page for entering completion dates. These dates are
    only here for product designers, that is, these are the MILESTONES for
    product designer and can be entered by product designer itself (only if the
    administrator rights are provided). Click Save Timeline Configuration.

  > Credential page. Enter names for customer and product designer for this
    product. These are auto_complete entries.

DESCRIPTION
-----------

The module is a bridge between product designer and the customer.
Product case tracker module is designed and developed keeping a
commissioned product in mind. This module is not,  in any way,
concerned with tracking the product after it has been purchased,
it keeps a track of the product-making and how far this
development has reached, via product status.

If a customer, on any given ecommerce site,
likes a product but is not satisfied with the
product color, design, shape, or look and feel, etc. of that product,
given that the customer has some potential interest in that product,
this product can be customized according to customer's indivisual
needs. So the customer orders for customizations in the product and
the ecommerce store/website can easily handle this commissioned
order by using this module.

The module provides a status bar, a diagrammatic view of the product
statuses, and provides a case history table which keeps a
track of all the history related to this product.
When a product is created, there is a 'View Case Tracker' link
available in /admin/content. Click this link to visit the 
case tracker of this commissioned product.

A demo of product development status that can be considered :
  -   Product enquiry by customer
  -   Contract Drafted by Product Designer
  -   Contract Accepted by Customer
  -   Initial Payment by Customer
  -   Delivery Agreement drafted
  -   Full Payment by Customer
  -   Product Delivered
  -   Balance Paid to Artisan



CASE TRACKER GUIDELINES
-----------------------

 * There must be a 'list(text)' field with 'select' widget in the content
type for ubercart's product class. Only if this field is selected in the 
settings page, case tracker will get started. This field will be editable and
valuable only in the case tracker page and nowhere else.

 * As soon as a product is created the super admin or the role with
the administrator rights (assuming this role is the product designer)
would be asked for timeline dates. The timeline dates are the actual
dates which the product designer promises to comply with.
Any deviation from these dates would be recorded in the timeline 
itself and reflect the product designer's performance.

 * Collaborators can be added or removed  by the product designer, and the
super admin only.

 * Only administrator, product designer, collaborators and customer of the
product can view the case tracker. No other user can access the case tracker
keeping in mind the security, and safeguarding the performance of product
designer from other product designers on the site.

 * Product designer can only be changed by the super admin.

 * Any given user can view his/her workspace at /user/<uid>.

 * There is a functionality to send an email with attachment from within the 
case tracker itself, if the customer or the product designer need to interact
via screenshots or product images.

 * Permissions to specific roles :
   -- For the product designer and customer roles, it is recommended
      that you provide 'View all commissioned orders', and 'Administer 
      timeline' permissions.
   -- You must provide 'Administer autocomplete entries' for product designer
      roles. This permission would only let the product designer access the
      other designers and customers on the site.

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
| ** TODO **                                                                  |
| Remove or hide the selected status field from node/<nid>/edit page, as it   |
| is currently of no use to the case tracker. Any changes made to this field  |
| in the node/edit page will be of no effect on the case tracker, only changes|
| made in the admin/<case_id>/product_case_tracker page will get reflected.   |
|                                                                             |
| ** TODO **                                                                  |
| To be more specific with auto_complete entries, that is, on entering values |
| in auto_complete fields, user picture, id, etc. should also appear so that  |
| the user is more identifiable.                                              |
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------


CREDITS
-------

* Developed and maintained by
  gauravjeet singh pahuja <gauravjeet at osscube dot com>

* Support and Guidance by
  bhupendra singh <bhupendra at osscube dot com>

* Product Timeline design assisted by
  abhijeet kalsi <abhijeet at osscube dot com>

* Sponsored by
  OSSCube
